package org.amicofragile.umtapo.people.web

import org.amicofragile.umtapo.web.WebConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.SpringApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.annotation.ComponentScan

object PeopleWebApp extends App {
  SpringApplication.run(classOf[PeopleWebApp])
}

@SpringBootApplication
@ComponentScan(basePackageClasses = Array(classOf[PeopleWebApp], classOf[WebConfiguration]))
class PeopleWebApp extends SpringBootServletInitializer {
  protected override def configure(application: SpringApplicationBuilder): SpringApplicationBuilder =
    application.sources(classOf[PeopleWebApp])
}

