package org.amicofragile.umtapo.people.web

import org.amicofragile.umtapo.core.es.{EventStore, InMemoryEventStore}
import org.amicofragile.umtapo.people.core.{EventStoreBasedPeopleRepository, InMemoryPeopleRepository, PeopleRepository}
import org.springframework.context.annotation.{Bean, Configuration}

@Configuration
class PeopleAppConfiguration {
  @Bean
  def eventStore : EventStore = new InMemoryEventStore()
  @Bean
  def repository(store: EventStore) : PeopleRepository = new EventStoreBasedPeopleRepository(store)
}
