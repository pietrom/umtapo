package org.amicofragile.umtapo.people.web.api

import java.net.URI
import java.time.ZonedDateTime
import java.util.UUID

import org.amicofragile.umtapo.core.Clock
import org.amicofragile.umtapo.people.core.{PeopleRepository, Person, PersonId}
import org.springframework.http.{HttpHeaders, HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation._

@RestController
@RequestMapping(Array("/api/people"))
class PeopleController(private val clock: Clock, private val repo: PeopleRepository) {
  @RequestMapping(path = Array(""), method = Array(RequestMethod.POST))
  //@ResponseStatus(HttpStatus.CREATED)
  def create(@RequestBody command: CreatePerson) : ResponseEntity[String] = {
    implicit val now:ZonedDateTime = clock.now
    val person = new Person(PersonId(UUID.randomUUID()), command.firstName, command.lastName)
    repo.save(person)

    val headers = new HttpHeaders()
    headers.setLocation(new URI("/api/people/" + person.id.id.toString))
    return new ResponseEntity[String]("", headers, HttpStatus.CREATED)
  }

  @RequestMapping(path = Array("{id}"), method = Array(RequestMethod.GET))
  def get(@PathVariable id : String) = {
    val person = repo.findById(PersonId(UUID.fromString(id)))
    person match {
      case None => throw new Exception("404")
      case Some(p) => p
    }
  }
}

case class CreatePerson(firstName: String, lastName: String)
