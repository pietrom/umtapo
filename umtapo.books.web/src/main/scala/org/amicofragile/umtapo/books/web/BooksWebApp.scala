package org.amicofragile.umtapo.books.web

import org.amicofragile.umtapo.web.WebConfiguration
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.ComponentScan

object BooksWebApp extends App {
  SpringApplication.run(classOf[BooksWebApp])
}

@SpringBootApplication
@ComponentScan(basePackageClasses = Array(classOf[BooksWebApp], classOf[WebConfiguration]))
class BooksWebApp extends SpringBootServletInitializer {
  protected override def configure(application: SpringApplicationBuilder): SpringApplicationBuilder =
    application.sources(classOf[BooksWebApp])
}
