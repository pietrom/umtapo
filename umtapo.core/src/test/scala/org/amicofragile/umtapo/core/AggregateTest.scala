package org.amicofragile.umtapo.core

import java.time.ZonedDateTime
import java.util.UUID

import org.junit.Assert.assertThat
import org.hamcrest.CoreMatchers.is
import org.hamcrest.CoreMatchers.equalTo
import org.amicofragile.umtapo.core.es.{Aggregate, DomainEvent, EventStream}
import org.junit.Test

class AggregateTest {
  private def now = ZonedDateTime.now()

  @Test
  def shouldReplayEventsReconstructingAggregateState = {
    val id = UserAccountId(UUID.randomUUID())
    val events = List(
      UserAccountCreated(now, id, "pietrom"),
      UserPreferenceAdded(now, UserPreference("k0", "v0")),
      UserPreferenceAdded(now, UserPreference("k1", "v1")),
      UserInfoUpdated(now, "Pedro", "Martinazzoli"),
      UserPreferenceAdded(now, UserPreference("k2", "v2")),
      UserInfoUpdated(now, "Peter", "Martin"),
      UserPreferenceAdded(now, UserPreference("k3", "v3")),
      UserInfoUpdated(now, "Pietro", "Martinelli"),
      UserPreferenceAdded(now, UserPreference("k4", "v4")),
    )
    val account = new UserAccount(new EventStream(events.size, events))
    assertThat(account.username, is(equalTo("pietrom")))
    assertThat(account.firstName, is(equalTo("Pietro")))
    assertThat(account.lastName, is(equalTo("Martinelli")))
    assertThat(account.preferences.size, is(equalTo(5)))
    assertThat(account.preferences(3).key, is(equalTo("k3")))
    assertThat(account.preferences(4).value, is(equalTo("v4")))
  }
}

class UserAccount(stream: EventStream) extends Aggregate(stream) {
  private var _id: UserAccountId = _
  private var _username: String = _
  private var _firstName: String = _
  private var _lastName: String = _
  private var _preferences: Seq[UserPreference] = _

  def username = _username
  def firstName = _firstName
  def lastName = _lastName
  def preferences = _preferences

  def this(id: UserAccountId, username: String)(implicit when: ZonedDateTime) = {
    this(EventStream.Empty)
    applyAndPublish(new UserAccountCreated(when, id, username))
  }

  private def on(evt: UserAccountCreated)= {
    _id = evt.id
    _username = evt.username
  }

  private def on(evt: UserPreferenceAdded) = {
    _preferences = Option(_preferences).orElse(Some(List[UserPreference]())).map(p => p :+ evt.preference).get
  }

  private def on(evt: UserInfoUpdated) = {
    _firstName = evt.firstName
    _lastName = evt.lastName
  }
}

case class UserAccountId(id: UUID)

case class UserPreference(key: String, value: String)

case class UserAccountCreated(occurredOn: ZonedDateTime, id: UserAccountId, username: String) extends DomainEvent

case class UserInfoUpdated(occurredOn: ZonedDateTime, firstName: String, lastName: String) extends DomainEvent

case class UserPreferenceAdded(occurredOn: ZonedDateTime, preference: UserPreference) extends DomainEvent