package org.amicofragile.umtapo.core

import org.junit.Test
import org.junit.Assert.assertThat
import org.hamcrest.CoreMatchers._

class FakeTest {
  @Test
  def fake: Unit = {
    assertThat(19, is(equalTo(19)))
  }
}
