package org.amicofragile.umtapo.core

import java.time.ZonedDateTime

import org.hamcrest.CoreMatchers.{is}
import org.hamcrest.Matchers.{greaterThanOrEqualTo, lessThanOrEqualTo}
import org.junit.Assert.assertThat
import org.junit.Test

class SystemClockTest {
  @Test
  def shouldReturnCurrentTimeAfterItial : Unit = {
    val now = ZonedDateTime.now().toInstant()
    assertThat(new SystemClock().now().toInstant(), is(greaterThanOrEqualTo(now)))
  }

  @Test
  def shouldReturnCurrentTimeBeforeFinal : Unit = {
    val current = new SystemClock().now().toInstant()
    val end = ZonedDateTime.now().toInstant()
    assertThat(current, is(lessThanOrEqualTo(end)))
  }
}
