package org.amicofragile.umtapo.core

import java.time.ZonedDateTime

class SystemClock extends Clock {
  def now() = ZonedDateTime.now()
}
