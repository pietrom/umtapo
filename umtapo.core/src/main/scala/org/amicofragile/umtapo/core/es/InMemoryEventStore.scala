package org.amicofragile.umtapo.core.es

class InMemoryEventStore extends EventStore {
  private var items = List[JournalItem]()

  override def loadEventStream(id: EventStreamId): EventStream = {
    val events = items.filter(it => it.matches(id))
      .sortBy(it => it.version)
      .map(it => it.event)
    new EventStream(events.size, events)
  }

  override def appendToStream(id: EventStreamId, events: List[DomainEvent], expectedVersion: Int) ={
    var currentVersion = expectedVersion
    for(evt <- events) {
      currentVersion = currentVersion + 1
      items = items :+ new JournalItem(id, currentVersion, evt)
    }
  }
}

class JournalItem(val id: EventStreamId, val version: Int, val event: DomainEvent) {
  def matches(id: EventStreamId) = {
    this.id.streamId.equals(id.streamId) && this.id.streamType.equals(id.streamType)
  }
}