package org.amicofragile.umtapo.core.es

import java.time.ZonedDateTime

trait DomainEvent {
  def occurredOn: ZonedDateTime
}

