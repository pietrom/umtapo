package org.amicofragile.umtapo.core.es

class StreamResource(events: List[DomainEvent], private var version: Int) {
  private var _originalVersion: Int = 0
  private var _changes = List[DomainEvent]()

  for (evt <- events) {
    apply(evt)
  }

  _originalVersion = version

  def originalVersion = _originalVersion

  def commit: Unit = {
    _changes = List[DomainEvent]()
    _originalVersion = version
  }

  def uncommittedChanges = _changes

  def this(stream: EventStream) = {
    this(stream.events, stream.version)
  }

  def this() = {
    this(List(), 1)
  }

  def apply(evt: DomainEvent) = {
    val method = getClass.getDeclaredMethod("on", evt.getClass)
    method.setAccessible(true)
    method.invoke(this, evt)
  }

  def applyAndPublish(evt: DomainEvent) = {
    apply(evt)
    _changes = _changes :+ evt
    version = version + 1
  }
}
