package org.amicofragile.umtapo.core

import java.time.ZonedDateTime

trait Clock {
  def now : ZonedDateTime
}
