package org.amicofragile.umtapo.core

import java.time.ZonedDateTime

class FixedClock (val now : ZonedDateTime) extends Clock
