package org.amicofragile.umtapo.core.es

class Aggregate(stream: EventStream) extends StreamResource(stream) {
  def this() = {
    this(EventStream.Empty)
  }
}
