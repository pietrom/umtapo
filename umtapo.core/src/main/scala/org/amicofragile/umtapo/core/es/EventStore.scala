package org.amicofragile.umtapo.core.es

trait EventStore {
  def loadEventStream(id: EventStreamId) : EventStream

  def appendToStream(aStartingIdentity: EventStreamId, events: List[DomainEvent], expectedVersion: Int)
}
