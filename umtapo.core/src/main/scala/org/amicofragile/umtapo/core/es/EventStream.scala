package org.amicofragile.umtapo.core.es

class EventStream(val version: Integer, val events: List[DomainEvent]) {
  def this() {
    this(0, List[DomainEvent]())
  }
}

object EventStream {
  def Empty = new EventStream(0, List[DomainEvent]())
}

