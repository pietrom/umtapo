package org.amicofragile.umtapo.people.core

import java.time.ZonedDateTime
import java.util.UUID

import org.amicofragile.umtapo.core.es.InMemoryEventStore
import org.junit.Test
import org.junit.Assert.assertThat
import org.junit.Assert.fail
import org.hamcrest.CoreMatchers.is

class EventStoreBasedPeopleRepositoryTest {
  @Test
  def canSaveAndReloadPerson: Unit = {
    val eventStore = new InMemoryEventStore
    val repo = new EventStoreBasedPeopleRepository(eventStore)
    val id = new PersonId(UUID.randomUUID())
    implicit val now = ZonedDateTime.now()
    val person = new Person(id, "Pietro", "Martinelli")
    repo.save(person)
    val reloaded = repo.findById(id)
    reloaded match {
      case None => fail()
      case Some(x) => {
        assertThat(x.firstName, is("Pietro"))
        assertThat(x.lastName, is("Martinelli"))
      }
    }
  }
}
