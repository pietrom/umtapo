package org.amicofagile.umtapo.people.core

import org.amicofragile.umtapo.core.es.InMemoryEventStore
import org.junit.Assert.assertThat
import org.junit.Test
import java.time.ZonedDateTime
import java.util.*
import org.hamcrest.CoreMatchers.`is`

class KEventStoreBasedPeopleRepositoryTest {
    @Test
    fun canSaveAndReloadPerson() {
        val eventStore = InMemoryEventStore()
        val repo = KEventStoreBasedPeopleRepository(eventStore)
        val id = KPersonId(UUID.randomUUID())
        val now = ZonedDateTime.now()
        val person = KPerson(id, "Pietro", "Martinelli", now)
        repo.save(person)
        val reloaded = repo.findById(id)
        assertThat(reloaded!!.firstName, `is`("Pietro"))
        assertThat(reloaded.lastName, `is`("Martinelli"))
    }
}