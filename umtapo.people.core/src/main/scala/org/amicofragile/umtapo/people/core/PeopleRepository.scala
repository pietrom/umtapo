package org.amicofragile.umtapo.people.core

import org.amicofragile.umtapo.core.es.{EventStore, EventStream, EventStreamId}

trait PeopleRepository {
  def save(person: Person) : Unit

  def findById(id: PersonId) : Option[Person]
}

class InMemoryPeopleRepository extends PeopleRepository {
  private var people : List[Person] = List()
  override def save(person: Person): Unit = {
    people = person :: people
  }

  override def findById(id: PersonId) = {
    people.find(p => p.id == id)
  }
}

class EventStoreBasedPeopleRepository(store: EventStore) extends PeopleRepository {
  override def save(person: Person): Unit = {
    val streamId = new EventStreamId("Person", person.id.id.toString)
    store.appendToStream(streamId, person.uncommittedChanges, person.originalVersion)
  }

  override def findById(id: PersonId ): Option[Person] = {
    val streamId = new EventStreamId("Person", id.id.toString())
    val stream = store.loadEventStream(streamId)
    val person = new Person(stream)
    return Some(person)

  }
}
