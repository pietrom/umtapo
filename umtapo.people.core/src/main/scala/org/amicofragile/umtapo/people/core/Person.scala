package org.amicofragile.umtapo.people.core

import java.time.ZonedDateTime
import java.util.UUID

import org.amicofragile.umtapo.core.es.{Aggregate, DomainEvent, EventStream}

class Person(stream: EventStream) extends Aggregate(stream) {
  private var _id : PersonId = _
  private var _firstName: String = _
  private var _lastName: String = _

  def this(id: PersonId, firstName: String, lastName: String)(implicit when: ZonedDateTime) = {
    this(EventStream.Empty)
    applyAndPublish(new PersonCreated(when, id, firstName, lastName))
  }

  private def on(event: PersonCreated) = {
    _id = event.id
    _firstName = event.firstName
    _lastName = event.lastName
  }

  def id = _id
  def firstName = _firstName
  def lastName = _lastName
}

case class PersonId(id: UUID)

case class PersonCreated(occurredOn:ZonedDateTime, id: PersonId, firstName: String, lastName: String)
  extends DomainEvent