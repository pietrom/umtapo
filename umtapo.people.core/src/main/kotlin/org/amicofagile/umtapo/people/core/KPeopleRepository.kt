package org.amicofagile.umtapo.people.core

import org.amicofragile.umtapo.core.es.EventStore
import org.amicofragile.umtapo.core.es.EventStreamId

interface KPeopleRepository {
    fun save(person: KPerson)

    fun findById(id: KPersonId) : KPerson?
}

class KEventStoreBasedPeopleRepository(val store: EventStore) : KPeopleRepository {
    override fun save(person: KPerson) {
        val streamId = EventStreamId("Person", person.id!!.id.toString())
        store.appendToStream(streamId, person.uncommittedChanges(), person.originalVersion())
    }

    override fun findById(id: KPersonId): KPerson? {
        val streamId = EventStreamId("Person", id.id.toString())
        val stream = store.loadEventStream(streamId)
        return KPerson(stream)
    }
}