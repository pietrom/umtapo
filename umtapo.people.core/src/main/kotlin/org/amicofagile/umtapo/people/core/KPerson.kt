package org.amicofagile.umtapo.people.core

import org.amicofragile.umtapo.core.es.Aggregate
import org.amicofragile.umtapo.core.es.DomainEvent
import org.amicofragile.umtapo.core.es.EventStream
import java.time.ZonedDateTime
import java.util.*

class KPerson(stream: EventStream) : Aggregate(stream) {
    var id : KPersonId? = null
        private set

    var firstName: String? = null
        private set

    var lastName: String? = null
        private set

    constructor(id: KPersonId, firstName: String, lastName: String, occurredOn: ZonedDateTime) : this(EventStream.Empty()) {
        applyAndPublish(KPersonCreated(occurredOn, id, firstName, lastName))
    }

    private fun on(event: KPersonCreated) {
        id = event.id
        firstName = event.firstName
        lastName = event.lastName
    }
}

data class KPersonId(val id: UUID)

abstract class KDomainEvent : DomainEvent {
    abstract val occurredOn: ZonedDateTime

    override fun occurredOn() : ZonedDateTime {
        return occurredOn
    }
}

data class KPersonCreated(override val occurredOn: ZonedDateTime, val id: KPersonId, val firstName: String, val lastName: String)
    : KDomainEvent()