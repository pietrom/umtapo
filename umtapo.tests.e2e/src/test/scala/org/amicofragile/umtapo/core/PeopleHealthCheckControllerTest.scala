package org.amicofragile.umtapo.core

import java.time.{ZoneId, ZonedDateTime}

import org.amicofragile.umtapo.people.web.PeopleWebApp
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.amicofragile.umtapo.web.healthcheck.{HealthCheckController, HealthCheckResult}
import org.junit.Test
import org.junit.Assert.assertThat
import org.mockito.Mockito.when
import org.hamcrest.CoreMatchers._
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort


@RunWith(classOf[SpringRunner])
@SpringBootTest(classes = Array(classOf[PeopleWebApp]), webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PeopleHealthCheckControllerTest {
  val Now = ZonedDateTime.of(1978, 3, 19, 1, 15, 0, 0, ZoneId.of("UTC+1"))

  @Autowired
  private val controller: HealthCheckController = null

  @LocalServerPort
  private val port: Int = 0

  @Autowired
  private val restTemplate: TestRestTemplate = null

  @MockBean
  private val clock: Clock = null

  @Test
  @throws[Exception]
  def contextCanBeLoaded(): Unit = {
    assertThat(controller, is(notNullValue))
  }

  @Test
  def healthCheckControllerReturnsOkStatus: Unit = {
    assertThat(
      restTemplate.getForObject("http://localhost:" + port + "/api/health-check", classOf[HealthCheckResult]).status,
      is(true)
    )
  }

  @Test
  def healthCheckControllerReturnsCurrentClockTime: Unit = {
    when(clock.now).thenReturn(Now)

    assertThat(
      restTemplate.getForObject("http://localhost:" + port + "/api/health-check", classOf[HealthCheckResult]).occurredOn.toInstant,
      is(equalTo(Now.toInstant))
    )
  }
}

object PeopleHealthCheckControllerTest {

}
