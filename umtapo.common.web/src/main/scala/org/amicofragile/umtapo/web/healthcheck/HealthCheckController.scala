package org.amicofragile.umtapo.web.healthcheck

import java.time.ZonedDateTime

import org.amicofragile.umtapo.core.Clock
import org.springframework.web.bind.annotation.{RequestMapping, RestController}

@RestController
@RequestMapping(Array("/api/health-check"))
class HealthCheckController(private val clock: Clock) {
  @RequestMapping(Array(""))
  def check = HealthCheckResult.ok(clock.now)
}

case class HealthCheckResult(occurredOn: ZonedDateTime, status: Boolean)

object HealthCheckResult {
  def ok(occurredOn: ZonedDateTime) = HealthCheckResult(occurredOn, true)
}
